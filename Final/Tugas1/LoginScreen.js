import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native'

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          userName: '',
          password: '',
          isError: false,
        }
    }
    loginHandler() {
        console.log(this.state.userName, ' ', this.state.password)
        if(this.state.password == '123'){
            this.props.navigation.navigate('DrawerScreen')
        }else{
            this.setState({isError: true})
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return(
            <KeyboardAvoidingView style={styles.container}>
                <View style={styles.title}>
                    <Image source={require('./logo/logo.png')} style={{width: 400, height: 100}}/>
                </View>
                <Text style={styles.login}>Login</Text>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.text}>Username/Email</Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder='Masukkan Nama User/Email'
                        onChangeText={userName => this.setState({ userName })}
                    />
                </View>
                <View style={{alignItems: 'center'}}>
                    <Text style={styles.text}>Password</Text>
                    <TextInput
                        style={styles.textInput}
                        placeholder='Masukkan Password'
                        onChangeText={password => this.setState({ password })}
                        secureTextEntry={true}
                    />
                </View>
                <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
                <TouchableOpacity onPress={() => this.loginHandler()}>
                    <View style={styles.button}>
                        <Text style={styles.text2}>Login</Text>
                    </View>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#79D9F8"
    },
    title: {
        height: 300,
        flexDirection: 'row',
        alignItems: 'center'
    },
    login: {
        marginTop: -20,
        fontFamily: 'sans-serif-medium',
        fontSize: 50,
        textAlign: 'center'
    },
    textInput: {
        width: 300,
        height: 30,
        borderRadius: 8,
        backgroundColor: 'white'
    },
    errorText: {
        color: 'red',
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    },
    text: {
        paddingTop: 35,
        fontSize: 20
    },
    box: {
        backgroundColor: 'white',
        height: 35,
        width: 300,
        justifyContent: 'center',
        borderRadius: 8,
        alignSelf: 'center'
    },
    button: {
        backgroundColor: '#0C1B6C',
        marginTop: 45,
        height: 45,
        width: 110,
        borderRadius: 8,
        alignSelf: 'center'
    },
    text2: {
        fontSize: 30,
        color: 'white',
        alignSelf: 'center'
    },
    bottom: {
        marginTop: 40,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    text3: {
        fontSize: 15
    },
    register: {
        fontSize: 15,
        color: 'red'
    }
})