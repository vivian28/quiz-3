import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, Button } from 'react-native'
const DEVICE = Dimensions.get('window')

export default class ListItem extends React.Component {

    currencyFormat(num) {
      return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
  
    //? #Soal No 3 (15 poin)
    //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device
  
    render() {
      const data = this.props.data
      return (
        <View style={styles.itemContainer}>
          <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
          <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
          <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
          <Button title='BELI' color='blue' />
        </View>
      )
    }
};

const styles = StyleSheet.create ({
    itemContainer: {
        width: DEVICE.width * 0.44,
    },
    itemImage: {
        height: 100,
        width: 200,
        alignSelf: 'center'
    },
    itemName: {
        fontSize: 18
    },
    itemPrice: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'blue',
        paddingTop: 10
    },
    itemStock: {
        alignSelf: 'center'
    },
    itemButton: {
    },
})